// Arthur Jordan
// October 4th, 2016
// CSC 444 - Assignment_6

//////////////////////////////////////////////////////////////////////////////
// Global variables, preliminaries

var svgSize = 500;
var bands = 50;

var xScale = d3.scaleLinear().domain([0, bands]).  range([0, svgSize]);
var yScale = d3.scaleLinear().domain([-1,bands-1]).range([svgSize, 0]);

var minTemp = d3.min(data, function(d) { return d.T; });
var maxTemp = d3.max(data, function(d) { return d.T; });

var minPressure = d3.min(data, function(d) { return d.P; });
var maxPressure = d3.max(data, function(d) { return d.P; });

function createSvg(sel) {
  return sel
    .append("svg")
    .attr("width", svgSize)
    .attr("height", svgSize);
}

function createRects(sel) {
  return sel
    .append("g")
    .selectAll("rect")
    .data(data)
    .enter()
    .append("rect")
    .attr("x", function(d) { return xScale(d.Col); })
    .attr("y", function(d) { return yScale(d.Row); })
    .attr("width", 10)
    .attr("height", 10);
}

function createPaths(sel) {
  return sel
    .append("g")
    .selectAll("g")
    .data(data)
    .enter()
    .append("g")
    .attr("transform", function(d) {
      return "translate(" + xScale(d.Col) + "," + yScale(d.Row) + ")";
    })
    .append("path");
}

d3.selection.prototype.callReturn = function(callable) {
    return callable(this);
};

//////////////////////////////////////////////////////////////////////////////

function glyphD(d) {
  var glyphOne = "M 5 5 L 5 0 M 5 5 L 0 5 M 5 5 L 5 10 M 5 5 L 10 5 M 5 5 L 10 10 M 5 5 L 0 0 M 5 5 L 10 0 M 5 5 L 0 10";
  var glyphTwo = "M 5 5 L 10 0 M 5 5 L 0 10 M 5 5 L 0 5 M 5 5 L 5 0 M 5 5 L 10 5 M 5 5 L 5 10";
  var glyphThree = "M 5 5 L 0 5 M 5 5 L 10 5 M 5 5 L 5 0 M 5 5 L 5 10";
  var glyphFour = "M 0 5 L 10 5";
  var glyphFive = "M 5 5 L 7 5 M 5 5 L 3 5";

  var glyphs = d3.scaleQuantize()
                 .domain([minPressure, maxPressure])
                 .range([glyphOne, glyphTwo, glyphThree, glyphFive, glyphFour]);
  return glyphs(d.P);
}

function glyphStroke(d) {
  if(d.P > 0) {
    return d3.rgb("white");
  } else {
    return d3.rgb("black");
  }
}

function colorT1(d) {
  // The following hex values are referenced from http://colorbrewer2.org/#type=diverging&scheme=RdYlGn&n=5
  var colorTemp = d3.scaleLinear()
                    .domain([minTemp, maxTemp])
                    .range(["#ffffcc", "#253494"]);

  return colorTemp(d.T);
}

 
function colorP1(d) {
  // The following interpolation is referenced from https://nelsonslog.wordpress.com/2011/04/11/d3-scales-and-interpolation
  // The following hex values are referenced from http://colorbrewer2.org/#type=diverging&scheme=RdYlGn&n=5
  var colorPressure = d3.scaleLinear()
                        .domain([-500, 0, 500])
                        .interpolate(d3.interpolateLab)
                        .range(["#e66101", "#f7f7f7", "#5e3c99"]);
  return colorPressure(d.P);
}

function colorPT(d) {
  // The following interpolation is referenced from https://nelsonslog.wordpress.com/2011/04/11/d3-scales-and-interpolation
  // The following hex values are referenced from http://colorbrewer2.org/#type=diverging&scheme=RdYlGn&n=5
  var pressureColor = d3.scaleLinear()
                        .domain([-500, 0, 500])
                        .interpolate(d3.interpolateLab)
                        .range(["#e66101", "#f7f7f7", "#5e3c99"]);

  // The following hex values are referenced from http://colorbrewer2.org/#type=diverging&scheme=RdYlGn&n=5
  var temperatureColor = d3.scaleLinear()
                           .domain([minTemp, maxTemp])
                           .range(["#ffffcc", "#253494"]);

  // The following interpolation is referenced from https://github.com/d3/d3-interpolate
  var bivariateColor = d3.interpolateLab(temperatureColor(d.T), pressureColor(d.P))(0.5);
  return d3.lab(bivariateColor);
}

function colorT2(d) {
  // The following hex values are referenced from http://colorbrewer2.org/#type=diverging&scheme=RdYlGn&n=5
  var color = d3.scaleLinear()
                .domain([minTemp, maxTemp])
                .range(["#feebe2", "#7a0177"]);
  return color(d.T);
}

//////////////////////////////////////////////////////////////////////////////

d3.select("#plot1-temperature")
  .callReturn(createSvg)
  .callReturn(createRects)
  .attr("fill", colorT1);

d3.select("#plot1-pressure")
  .callReturn(createSvg)
  .callReturn(createRects)
  .attr("fill", colorP1);

d3.select("#plot2-bivariate-color")
  .callReturn(createSvg)
  .callReturn(createRects)
  .attr("fill", colorPT);

var bivariateSvg = d3.select("#plot3-bivariate-glyph")
                     .callReturn(createSvg);

bivariateSvg
    .callReturn(createRects)
    .attr("fill", colorT2);

bivariateSvg
    .callReturn(createPaths)
    .attr("d", glyphD)
    .attr("stroke", glyphStroke)
    .attr("stroke-width", 1);
