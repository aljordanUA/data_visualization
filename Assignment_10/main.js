//////////////////////////////////////////////////////////////////////////////
// Global variables, preliminaries

var svgSize = 500;
var bands = 50;

var xScale = d3.scaleLinear().domain([0, bands]).  range([0, svgSize]);
var yScale = d3.scaleLinear().domain([-1,bands-1]).range([svgSize, 0]);

function createSvg(sel) {
  return sel
      .append("svg")
      .attr("width", svgSize)
      .attr("height", svgSize);
}

function vectorMagnitude(x, y) {
  var magnitude = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
  return magnitude;
};

var magnitudeScale = d3.scaleLinear()
                       .domain([0, d3.max( data, function(d) { return vectorMagnitude(d.vx, d.vy); })])
                       .range([5, 10]);

function createGroups(data) {
  return function(sel) {
      return sel
          .append("g")
          .selectAll("*")
          .data(data)
          .enter()
          .append("g")
          .attr("transform", function(d) {
              return "translate(" + xScale(d.Col) + "," + yScale(d.Row) + ")";
          });
  };
}

d3.selection.prototype.callReturn = function(callable) {
  return callable(this);
};

//////////////////////////////////////////////////////////////////////////////// PART 1
var colorScale = d3.scaleLinear()
                   .domain([0, 2])
                   .range(["white", "red"]);

var magColor = d3.select("#plot1-color")
                 .callReturn(createSvg)
                 .callReturn(createGroups(data));

magColor.append("rect")
        .attr("height", 10)
        .attr("width", 10)
        .attr("fill", function(d){ return colorScale(Math.sqrt(Math.pow(d.vx, 2) + Math.pow(d.vy, 2))); });

//////////////////////////////////////////////////////////////////////////////// PART 2
var hedgehog = d3.select("#plot1-hedgehog")
                 .callReturn(createSvg)
                 .callReturn(createGroups(data));

var lineScale = d3.scaleLinear()
                  .domain([-1, 0, 1])
                  .range([0, 5, 10]);

hedgehog.append("line")
        .attr("x1", 5)
        .attr("x2", function(d){ return 5 + d.vx * 8; })
        .attr("y1", 5)
        .attr("y2", function(d){ return 5 + d.vy * 8; })
        .attr("stroke", "black");

//////////////////////////////////////////////////////////////////////////////// PART 3
var unifGlyph = d3.select("#plot1-uniform")
                  .callReturn(createSvg)
                  .callReturn(createGroups(data));

 unifGlyph.append("g")
          .append("path")
          .attr("d", function(d) {
            var magnitude = vectorMagnitude(d.vx, d.vy);
            return "M 5 5 L " + 2 * magnitudeScale(magnitude) + " 5 L " + 2 * magnitudeScale(magnitude) + " 6 L " + ( 2 * magnitudeScale(magnitude) + 2) + " 5 L " + 2 * magnitudeScale(magnitude) + " 4 L " + 2 * magnitudeScale(magnitude) + " 5 Z";})
          .attr("transform", function(d) { return "rotate(" + ((Math.atan2(d.vy, d.vx)/Math.PI) * 180) +  " , 5 , 5)"; })
          .attr("stroke", "black");

//////////////////////////////////////////////////////////////////////////////// PART 4

function randomPoints() {
  return Math.random() * (10 - 0);
}

var randomGlyph = d3.select("#plot1-random")
                    .callReturn(createSvg)
                    .callReturn(createGroups(data));

randomGlyph.append("g")
           .append("path")
           .attr("d", function(d) {
               d.randomXPoint = randomPoints();
               d.randomYPoint = randomPoints();
              var magnitude = vectorMagnitude(d.vx, d.vy);
              return "M " + d.randomXPoint + " " + d.randomYPoint + " L " + (d.randomXPoint + magnitudeScale(magnitude)) + " " + (d.randomYPoint) + " L " + (d.randomXPoint + magnitudeScale(magnitude)) + " " + (d.randomYPoint - 1) + 
                     " L " + (d.randomXPoint + magnitudeScale(magnitude) + 2) + " " + (d.randomYPoint) + " L " + (d.randomXPoint + magnitudeScale(magnitude)) + " " + (d.randomYPoint + 1) + " L " + (d.randomXPoint + magnitudeScale(magnitude)) + 
                     " " + d.randomYPoint + " Z";})
           .attr("transform", function(d) { return "rotate(" + ((Math.atan2(d.vy, d.vx)/Math.PI) * 180) +  " , " + d.randomXPoint + ", " + d.randomYPoint + ")"; })
           .attr("stroke", "black");