// Arthur Jordan
// September 27th, 2016
// CSC 444 - Assignment_5

var xScale = d3.scaleLinear().domain([300, 800]).range([100, 400]);
var yScale = d3.scaleLinear().domain([200, 800]).range([400, 10]);
var radiusScale = d3.scaleSqrt().domain([15, 35]).range([10, 20]);

var originalCircleColor = d3.scaleLinear().domain([200, 800]).range(["white", "black"]);
var circleColorOne = d3.scaleLinear().domain([200, 800]).range(["red", "green"]);
var circleColorTwo = d3.scaleLinear().domain([200, averageSATV(scores), 800]).range([d3.rgb("#d7191c"), d3.rgb("#ffffbf"), d3.rgb("#1a9641")]);
var circleColorThree = d3.scaleQuantize().domain([200, 800]).range([d3.rgb("#d7191c"), d3.rgb("#fdae61"), d3.rgb("#ffffbf"), d3.rgb("#a6d96a"), d3.rgb("#1a9641")]);

var mySvg = d3.select("#scatterplot_1").append("svg").attr("width", 500).attr("height", 500);

// The following code is referenced from http://bl.ocks.org/mbostock/02d893e3486c70c4475f
mySvg.append("g")
		 .attr("class", "axis axis--x")
		 .attr("transform", "translate(0, 480)")
		 .call(d3.axisBottom(xScale));

mySvg.append("g")
     .attr("class", "axis axis--y")
     .attr("transform", "translate(30, -5)")
     .call(d3.axisLeft(yScale));

var myCircles = mySvg.selectAll("circle")
										 .data(scores)
										 .enter()
										 .append("circle")
										 .attr("cx", function(scores) 	{ return xScale(scores.SATM); })
										 .attr("cy", function(scores) 	{ return yScale(scores.SATV); })
										 .attr("r", function(scores)  	{ return radiusScale(scores.ACT); })
										 .attr("fill", function(scores) { return originalCircleColor(scores.SATM); })
										 .attr("stroke", "none");

function averageSATV(scores) {
	var count = 0;
	for(var i = 0; i < scores.length; ++i) {
		count += scores[i].SATV;
	}
	return count / scores.length;
}