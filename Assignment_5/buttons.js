// Arthur Jordan
// September 27th, 2016
// CSC 444 - Assignment_5

function updateDataOne() {
  myCircles.transition().duration(3000).attr("fill", function(scores) { return circleColorOne(scores.SATV); });
}

function updateDataTwo() {
  myCircles.transition().duration(3000).attr("fill", function(scores) { return circleColorTwo(scores.SATV); });
}

function updateDataThree() {
  myCircles.transition().duration(3000).attr("fill", function(scores) { return circleColorThree(scores.SATV); });
}

// The following code was referenced from https://cscheid.net/courses/fal16/cs444/assignment_5/buttons.html
var buttonList = [
  {
    name: "colormap-button-1",
    text: "Button_1",
    click: function() { 
      updateDataOne(); 
    }
  },
  {
    name: "colormap-button-2",
    text: "Button_2",
    click: function() { 
      updateDataTwo();  
    }
  },
  {
    name: "colormap-button-3",
    text: "Button_3",
    click: function() { 
      updateDataThree(); 
    }
  }
];

d3.select("#main")
  .selectAll("button")
  .data(buttonList)
  .enter()
  .append("button")
  .attr("id", function(d)  { return d.name; })
  .text(function(d)        { return d.text; })
  .on("click", function(d) { return d.click(); });