// Arthur Jordan
// October 18th, 2016
// CSC 444 - Assignment_7

var data = scores;
var brush1 = null;
var brush2 = null;

function setText(point) {
  d3.select("#table-SATM").text(point.SATM);
  d3.select("#table-SATV").text(point.SATV);
  d3.select("#table-ACT").text(point.ACT);
  d3.select("#table-GPA").text(point.GPA);
}

function updateRadius(firstPoint) {
  var allCircles = d3.select("body").selectAll("circle");
  allCircles.attr("r", 
  function(secondPoint) { 
    if(firstPoint == secondPoint) {
      return 10;
    } else {
      return 3;
    }
  });
}

function makeScatterplot(sel, xAccessor, yAccessor) {
  
  var svg = sel.append("svg")
               .attr("width", 500)
               .attr("height", 500);

  var xExtent = d3.extent(data, xAccessor);
  var yExtent = d3.extent(data, yAccessor);

  // create a scale for the x axis
  var xScale = d3.scaleLinear()
                 .domain(xExtent)
                 .range([50, 490]);

  // create a scale for the y axis
  var yScale = d3.scaleLinear()
                 .domain(yExtent)
                 .range([480, 10]);

  var brush = d3.brush();

  svg.append("g")
     .attr("class", "brush")
     .call(brush);
  
  // finish writing the circle creation here.
  // this *must* be done *after* adding the brush group.
  svg.append("g")
     .selectAll("circle")
     .data(data)
     .enter()
     .append("circle")
     .attr("cx",  function(data) { return xScale(xAccessor(data)); })
     .attr("cy",  function(data) { return yScale(yAccessor(data)); })
     .on("click", function(data) { setText(data); updateRadius(data); })
     .attr("r",  3)
     .attr("fill", "black");
  
  // create an axis object for the x axis
  var xAxis = svg.append("g")
                 .attr("class", "axis axis--x")
                 .attr("transform", "translate(0, 480)")
                 .call(d3.axisBottom(xScale));

  // create an axis object for the y axis
  var yAxis = svg.append("g")
                 .attr("class", "axis axis--y")
                 .attr("transform", "translate(50, 0)")
                 .call(d3.axisLeft(yScale));

  return {
    svg: svg,
    brush: brush,
    xScale: xScale,
    yScale: yScale
  };
}

//////////////////////////////////////////////////////////////////////////////

plot1 = makeScatterplot(d3.select("#scatterplot_1"),
                        function(d) { return d.SATM; },
                        function(d) { return d.SATV; });
plot2 = makeScatterplot(d3.select("#scatterplot_2"),
                        function(d) { return d.ACT; },
                        function(d) { return d.GPA; });

//////////////////////////////////////////////////////////////////////////////

function onBrush(d) {
    var allCircles = d3.select("body").selectAll("circle");
    if (brush1 === null && brush2 === null) {
      allCircles.attr("fill", "black");
      return;
    }
    
    // function isSelected(d) {

    //   if(brush1 != null) {
    //     var brushOneReturn = brush1[0][0] < plot1.xScale(d.SATM) && plot1.xScale(d.SATM) < brush1[1][0] &&
    //                          brush1[0][1] < plot1.yScale(d.SATV) && plot1.yScale(d.SATV) < brush1[1][1];
    //   }

    //   if(brush2 != null) {
    //     var brushTwoReturn = brush2[0][0] < plot2.xScale(d.ACT) && plot2.xScale(d.ACT) < brush2[1][0] &&
    //                          brush2[0][1] < plot2.yScale(d.GPA) && plot2.yScale(d.GPA) < brush2[1][1];
    //   }

    //   if(brush1 != null && brush2 == null) {
    //     return brushOneReturn;
    //   } else if(brush2 != null && brush1 == null) {
    //     return brushTwoReturn;
    //   } else {
    //     return brushOneReturn && brushTwoReturn;
    //   }
    // }

    function isSelected(d) {

      if(brush1 != null && brush2 == null) {
        var brushOneReturn = brush1[0][0] < plot1.xScale(d.SATM) && plot1.xScale(d.SATM) < brush1[1][0] &&
                             brush1[0][1] < plot1.yScale(d.SATV) && plot1.yScale(d.SATV) < brush1[1][1];
        return brushOneReturn;

      } else if(brush2 != null && brush1 == null) {

        var brushTwoReturn = brush2[0][0] < plot2.xScale(d.ACT) && plot2.xScale(d.ACT) < brush2[1][0] &&
                             brush2[0][1] < plot2.yScale(d.GPA) && plot2.yScale(d.GPA) < brush2[1][1];
        return brushTwoReturn;

      } else {
        var brushOneReturn = brush1[0][0] < plot1.xScale(d.SATM) && plot1.xScale(d.SATM) < brush1[1][0] &&
                             brush1[0][1] < plot1.yScale(d.SATV) && plot1.yScale(d.SATV) < brush1[1][1];

        var brushTwoReturn = brush2[0][0] < plot2.xScale(d.ACT) && plot2.xScale(d.ACT) < brush2[1][0] &&
                             brush2[0][1] < plot2.yScale(d.GPA) && plot2.yScale(d.GPA) < brush2[1][1];
        return brushOneReturn && brushTwoReturn;
      }

      // if(brush1 != null && brush2 == null) {
      //   return brushOneReturn;
      // } else if(brush2 != null && brush1 == null) {
      //   return brushTwoReturn;
      // } else {
      //   return brushOneReturn && brushTwoReturn;
      // }
    }
    
    var selected = allCircles
            .filter(isSelected)
            .attr("fill", "red");

    var notSelected = allCircles
            .filter(function(d) { return !isSelected(d); })
            .attr("fill", "lightgray");
}

//////////////////////////////////////////////////////////////////////////////
//
// d3 brush selection
//
// The "selection" of a brush is the range of values in either of the
// dimensions that an existing brush corresponds to. The brush selection
// is available in the d3.event.selection object.
// 
//   e = d3.event.selection
//   e[0][0] is the minimum value in the x axis of the brush
//   e[1][0] is the maximum value in the x axis of the brush
//   e[0][1] is the minimum value in the y axis of the brush
//   e[1][1] is the maximum value in the y axis of the brush
//
// The most important thing to know about the brush selection is that
// it stores values in *PIXEL UNITS*. Your logic for highlighting
// points, however, is not based on pixel units: it's based on data
// units.
//
// In order to convert between the two of them, remember that you have
// the d3 scales you created with the makeScatterplot function above.
// The final thing you need to know is that d3 scales have a function
// to *invert* a mapping: if you create a scale like this:
//
//  s = d3.scaleLinear().domain([5, 10]).range([0, 100])
//
// then s(7.5) === 50, and s.invert(50) === 7.5. In other words, the
// scale object has a method invert(), which converts a value in the
// range to a value in the domain. This is exactly what you will need
// to use in order to convert pixel units back to data units.

function updateBrush1() {
  brush1 = d3.event.selection;
  onBrush(data);
}

function updateBrush2() {
  brush2 = d3.event.selection;
  onBrush(data);
}

plot1.brush
     .on("brush", updateBrush1)
     .on("end", updateBrush1);

plot2.brush
     .on("brush", updateBrush2)
     .on("end", updateBrush2);