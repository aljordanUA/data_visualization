// Arthur Jordan
// September 20th, 2016
// CSC 444 - Assignment_4

// Select div with id="chart1", append an svg with height, width = 400 and class = "my-chart".
var xScale = d3.scaleLinear().domain([1969, 1984 + 1]).range([0, 600]);
var yScale = d3.scaleLinear().domain([0, 11]).range([275, 0]);

var firstSvg = d3.select("#chart1").append("svg").attr("width", 600).attr("height", 300).attr("class", "my-chart");
firstSvg.
selectAll("rect").
data(ukDriverFatalities).
enter().
append("rect").
attr("width", 38).
attr("height", 25).
attr("x", function(ukDriverFatalities)      { return xScale(ukDriverFatalities.year); }).
attr("y", function(ukDriverFatalities)      { return yScale(ukDriverFatalities.month); }).
attr("fill", function(ukDriverFatalities)   { return color(ukDriverFatalities.count); });

//Select div with id="chart2", append an svg with height, width = 400 and class = "my-chart".
var xScaleOne = d3.scaleLinear().domain([1969, 1984 + 1]).range([19, 627]);
var yScaleOne = d3.scaleLinear().domain([0, 11]).range([287.5, 12.5]);

var secondSvg = d3.select("#chart2").append("svg").attr("width", 600).attr("height", 300).attr("class", "my-chart");
secondSvg.
selectAll("circle").
data(ukDriverFatalities).
enter().
append("circle").
attr("cx", function(ukDriverFatalities)     { return xScaleOne(ukDriverFatalities.year); }).
attr("cy", function(ukDriverFatalities)     { return yScaleOne(ukDriverFatalities.month); }).
attr("r", function(ukDriverFatalities)      { return (ukDriverFatalities.count / 500 * 3); }).
attr("fill", "blue").
attr("stroke", "white");

// Select div with id="chart3", append an svg with height, width = 400 and class = "my-chart".
var heightScale = d3.scaleLinear().domain([0, 2500]).range([0, 300]);
var thirdSvg = d3.select("#chart3").append("svg").attr("width", 600).attr("height", 300).attr("class", "my-chart");
thirdSvg.
selectAll("rect").
data(ukDriverFatalities).
enter().
append("rect").
attr("width", 4).
attr("height", function(ukDriverFatalities) { return heightScale(ukDriverFatalities.count); }).
attr("x", function(ukDriverFatalities, i)   { return (i * 600 / ukDriverFatalitiesLength); }).
attr("y", function(ukDriverFatalities)      { return 300 - (heightScale(ukDriverFatalities.count)); });

// Select div with id="scatterplot_1", append an svg with height, width = 400 and class = "my-chart".
var radiusScale = d3.scaleLinear().domain([15, 36]).range([3, 7]);
var xScaleThree = d3.scaleLinear().domain([0, 800]).range([0, 490]);
var yScaleThree = d3.scaleLinear().domain([0, 800]).range([500, 10]);

var fourthSvg = d3.select("#scatterplot_1").append("svg").attr("width", 500).attr("height", 500);
fourthSvg.
selectAll("circle").
data(scores).
enter().
append("circle").
attr("cx", function(scores)       { return xScaleThree(scores.SATM); }).
attr("cy", function(scores)       { return yScaleThree(scores.SATV); }).
attr("r", function(scores)        { return radiusScale(scores.ACT); }).
attr("fill", function(scores)     { return gpaCircleColor(scores.GPA); }).
attr("stroke", "black");

// This function was taken from the file iteration_8.js presented in class.
function toHex(v) {
  var str = "00" + Math.floor(Math.max(0, Math.min(255, v))).toString(16);
  return str.substr(str.length-2);
}

// This function was taken from the file iteration_8.js presented in class.
function color(count) {
  var amount = (2500 - count) / 2500 * 255;
  var s = toHex(amount), s2 = toHex(amount / 2 + 127), s3 = toHex(amount / 2 + 127);
  return "#" + s + s2 + s3;
}

// This function returns a string corresponding to the fill color of the circle.
function gpaCircleColor(gpa) {
  var color = d3.scaleLinear().domain([1.0, 4.0]).range(["red", "green"]);
  returnColor = color(gpa);
  return returnColor;
}
