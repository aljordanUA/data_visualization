// HINT: Consider starting with a smaller, hand-written version of a tree, instead
// of the one in flare.js.

var data = flare;

//////////////////////////////////////////////////////////////////////////////

function setTreeSize(tree) {
  if(tree.children !== undefined) {
    var size = 0;
    for (var i=0; i<tree.children.length; ++i) {
      size += setTreeSize(tree.children[i]);
    }
    tree.size = size;
  }

  if(tree.children === undefined) {
    // do nothing, tree.size is already defined for leaves
  }
  return tree.size;
};

function setTreeCount(tree) {

  if(tree.children !== undefined) {
    var count = 0;
    for (var i=0; i<tree.children.length; ++i) {
      count += setTreeCount(tree.children[i]);
    }
    tree.count = count;
  }
  if(tree.children === undefined) {
    tree.count = 1;
  }
  return tree.count;
}

/*********************************** WRITE THIS PART. Use the code above as example *******************************/
var depths = [];
function setTreeDepth(tree, depth) {

  if(tree.children !== undefined) {
    tree.depth = depth;
    for (var i=0; i<tree.children.length; ++i) {
      depth += setTreeDepth(tree.children[i], tree.depth + 1);
    }
  } else {
    tree.depth = depth;
  }

  depths.push(tree.depth);
  return tree.depth;
};

setTreeSize(data);
setTreeCount(data);
var maxDepth = setTreeDepth(data, 0);

//////////////////////////////////////////////////////////////////////////////
// THIS IS THE MAIN CODE FOR THE TREEMAPPING TECHNIQUE
function setRectangles(rect, tree, attrFun) {
  var i;
  tree.rect = rect;

  if(tree.children !== undefined) {
    var cumulativeSizes = [0];
    for (i=0; i<tree.children.length; ++i) {
      cumulativeSizes.push(cumulativeSizes[i] + attrFun(tree.children[i]));
    }
    var height = rect.y2 - rect.y1, width = rect.x2 - rect.x1;
    var scale = d3.scaleLinear()
                  .domain([0, cumulativeSizes[cumulativeSizes.length-1]]);

    var border = 5;
    
    /************************************ WRITE THIS PART *********************************************************/
    for (i=0; i<tree.children.length; ++i) {
      var newRect;
      if(tree.depth % 2 == 0) {
        scale.range([tree.rect.x1, tree.rect.x2]);
        newRect = { x1: scale(cumulativeSizes[i]) + border, x2: scale(cumulativeSizes[i+1]) - border, y1: rect.y1 + border, y2: rect.y2 - border};
      } else {
        scale.range([tree.rect.y1, tree.rect.y2]);
        newRect = { x1: rect.x1 + border, x2: rect.x2 - border, y1: scale(cumulativeSizes[i]) + border, y2: scale(cumulativeSizes[i+1]) - border};
      }
      setRectangles(newRect, tree.children[i], attrFun);
    }
  }
}

function setRectanglesSize(rect, tree, attrFun) {
  var i;
  tree.rect = rect;

  if(tree.children !== undefined) {
    var cumulativeSizes = [0];
    for (i=0; i<tree.children.length; ++i) {
      cumulativeSizes.push(cumulativeSizes[i] + attrFun(tree.children[i]));
    }
    var height = rect.y2 - rect.y1, width = rect.x2 - rect.x1;
    var scale = d3.scaleLinear()
                  .domain([0, cumulativeSizes[cumulativeSizes.length-1]]);

    var border = 5;
    
    /************************************ WRITE THIS PART ************************************/ 
    // hint: set the range of the "scale" variable above appropriately,
    // depending on the shape of the current rectangle.
    for (i=0; i<tree.children.length; ++i) {
      var newRect;
      if(rect.x2 - rect.x1 > rect.y2 - rect.y1) {
        scale.range([rect.x1, rect.x2]);
        newRect = { x1: scale(cumulativeSizes[i]) + border, x2: scale(cumulativeSizes[i+1]) - border, y1: rect.y1 + border, y2: rect.y2 - border};
      } else {
        scale.range([rect.y1, rect.y2]);
        newRect = { x1: rect.x1 + border, x2: rect.x2 - border, y1: scale(cumulativeSizes[i]) + border, y2: scale(cumulativeSizes[i+1]) - border};
      }
      setRectanglesSize(newRect, tree.children[i], attrFun);
    }
  }
}

function setRectanglesCount(rect, tree, attrFun) {
  var i;
  tree.rect = rect;

  if(tree.children !== undefined) {
    var cumulativeSizes = [0];
    for (i=0; i<tree.children.length; ++i) {
      cumulativeSizes.push(cumulativeSizes[i] + attrFun(tree.children[i]));
    }
    var height = rect.y2 - rect.y1, width = rect.x2 - rect.x1;
    var scale = d3.scaleLinear()
                  .domain([0, cumulativeSizes[cumulativeSizes.length-1]])
                  .range([rect.x1, rect.x2]);

    var border = 5;
    
    /************************************ WRITE THIS PART ************************************/ 
    // hint: set the range of the "scale" variable above appropriately,
    // depending on the shape of the current rectangle.
    for (i=0; i<tree.children.length; ++i) {
      var newRect;
      if(rect.x2 - rect.x1 > rect.y2 - rect.y1) {
        scale.range([rect.x1, rect.x2]);
        newRect = { x1: scale(cumulativeSizes[i]) + border, x2: scale(cumulativeSizes[i+1]) - border, y1: rect.y1 + border, y2: rect.y2 - border};
      } else {
        scale.range([rect.y1, rect.y2]);
        newRect = { x1: rect.x1 + border, x2: rect.x2 - border, y1: scale(cumulativeSizes[i]) + border, y2: scale(cumulativeSizes[i+1]) - border};
      }
      setRectanglesCount(newRect, tree.children[i], attrFun);
    }
  }
}

var width = window.innerWidth;
var height = window.innerHeight;

setRectangles(
  {x1: 0, x2: width, y1: 0, y2: height}, data,
  function(t) { return t.size; }
);

function makeTreeNodeList(tree, lst) {
  lst.push(tree);
  if(tree.children !== undefined) {
    for(var i=0; i<tree.children.length; ++i) {
      makeTreeNodeList(tree.children[i], lst);
    }
  }
}

var treeNodeList = [];
makeTreeNodeList(data, treeNodeList);

var gs = d3.select("#svg")
           .attr("width", width)
           .attr("height", height)
           .selectAll("g")
           .data(treeNodeList)
           .enter()
           .append("g");

function setColor(node) {
  var greatestDepth = d3.max(depths, function(treeNode) {return treeNode;});
  var color = d3.scaleLinear().domain([0, greatestDepth]).range(["#d7191c", "#2c7bb6"]);
  return color(node.depth);
}

function setAttrs(sel) {
  /************************************ WRITE THIS PART ************************************/
  sel.attr("width",   function(treeNode) {return treeNode.rect.x2 - treeNode.rect.x1;})
     .attr("height",  function(treeNode) {return treeNode.rect.y2 - treeNode.rect.y1;})
     .attr("x",       function(treeNode) {return treeNode.rect.x1;})
     .attr("y",       function(treeNode) {return treeNode.rect.y1;})
     .attr("fill",    function(treeNode) {return setColor(treeNode);})
     .attr("stroke",  function(treeNode) {return "black";})
     .attr("title",   function(treeNode) {return treeNode.name;});
}

gs.append("rect").call(setAttrs);

d3.select("#size").on("click", function() {
  setRectangles(
    {x1: 0, x2: width, y1: 0, y2: height}, data,
    function(t) {return t.size;}
  );
  d3.selectAll("rect").transition().duration(1000).call(setAttrs);
});

d3.select("#best-size").on("click", function() {
  setRectanglesSize(
    {x1: 0, x2: width, y1: 0, y2: height}, data,
    function(t) {return t.size;}
  );
  d3.selectAll("rect").transition().duration(1000).call(setAttrs);
});

d3.select("#count").on("click", function() {
  setRectangles(
    {x1: 0, x2: width, y1: 0, y2: height}, data,
    function(t) {return t.count;}
  );
  d3.selectAll("rect").transition().duration(1000).call(setAttrs);
});

d3.select("#best-count").on("click", function() {
  setRectanglesCount(
    {x1: 0, x2: width, y1: 0, y2: height}, data,
    function(t) {return t.count;}
  );
  d3.selectAll("rect").transition().duration(1000).call(setAttrs);
});