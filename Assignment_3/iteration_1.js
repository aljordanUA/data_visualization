// Arthur Jordan
// September 13th, 2016
// CSC 444 - Assignment_3

// This function was taken from the provided file svg.js
function make(name, attrs) {
  var element = document.createElementNS("http://www.w3.org/2000/svg", name);
  if (attrs === undefined) attrs = {};
  for (var key in attrs) {
    element.setAttributeNS(null, key, attrs[key]);
  }
  return element;
}

// This function was taken from the provided file svg.js
function toHex(v) {
  var str = "00" + Math.floor(Math.max(0, Math.min(255, v))).toString(16);
  return str.substr(str.length-2);
}

// This function was taken from the provided file svg.js
function rgb(r, g, b) {
  return "#" + toHex(r * 255) + toHex(g * 255) + toHex(b * 255);
}

function plotAllOne(svg, data, xGetter, yGetter, radiusGetter) {
  for (var i = 0; i < data.length; ++i) {
    svg.appendChild(
      make("circle", {
        cx: xGetter(data[i], i),
        cy: yGetter(data[i], i),
        r: radiusGetter(data[i]),
        fill: gpaCircleColor(data[i].GPA),
        stroke: "black"
    }));
  }
}

function plotAllTwo(svg, data, xGetter, yGetter, radiusGetter) {
  for (var i = 0; i < data.length; ++i) {
    svg.appendChild(
      make("circle", {
        cx: xGetter(data[i], i),
        cy: yGetter(data[i], i),
        r: radiusGetter(data[i]),
        fill: satvCircleColor(data[i].SATV),
        stroke: "black"
    }));
  }
}

function plotAllThree(svg, data, xGetter, yGetter, radiusGetter) {
  for (var i = 0; i < data.length; ++i) {
    svg.appendChild(
      make("circle", {
        cx: xGetter(data[i], i),
        cy: yGetter(data[i], i),
        r: radiusGetter(data[i]),
        fill: actCircleColor(data[i].ACT),
        stroke: "black"
    }));
  }
}

function gpaCircleColor(gpa) {
  var circleFillColor = "";
  if (gpa < 4.0 && gpa >= 3.0) {
    circleFillColor = rgb(0,16,0);
  } else if (gpa < 3.0 && gpa >= 2.0) {
    circleFillColor = rgb(16,16,0);
  } else {
    circleFillColor = rgb(16,0,0);
  }
  return circleFillColor;
}

function satvCircleColor(satv) {
  var circleFillColor = "";
  if (satv < 800 && satv >= 600) {
    circleFillColor = rgb(0,16,0);
  } else if (satv < 600 && satv >= 400) {
    circleFillColor = rgb(16,16,0);
  } else {
    circleFillColor = rgb(16,0,0);
  }
  return circleFillColor;
}

function actCircleColor(act) {
  var circleFillColor = "";
  if (act < 40 && act >= 30) {
    circleFillColor = rgb(0,16,0);
  } else if (act < 30 && act >= 20) {
    circleFillColor = rgb(16,16,0);
  } else {
    circleFillColor = rgb(16,0,0);
  }
  return circleFillColor;
}

var scatterOne = make("svg", { width: 500, height: 500, "class": "my-chart" });
document.getElementById("scatterplot_1").appendChild(scatterOne);
plotAllOne(
  scatterOne, 
  scores, 
  function(row, index) { return (row.SATM * (1/2)) - 50; },
  function(row, index) { return 500 - ((row.SATV * (1/2)) - 25); },
  function(row, index) { return (row.ACT / 5); }
);

var scatterTwo = make("svg", { width: 500, height: 500, "class": "my-chart" });
document.getElementById("scatterplot_2").appendChild(scatterTwo);
plotAllTwo(
  scatterTwo,
  scores,
  function(row, index) { return (row.ACT * (50/4)); },
  function(row, index) { return 500 - (row.GPA * (500/5)); },
  function(row, index) { return (row.SATV * (3/520)); }
  );

var scatterThree = make("svg", {width: 500, height: 500, "class": "my-chart"});
document.getElementById("scatterplot_3").appendChild(scatterThree);
plotAllThree(
  scatterThree,
  scores,
  function(row, index) { return ((row.SATV + row.SATM) * (5/16)); },
  function(row, index) { return 500 - (row.GPA * (500/5)); },
  function(row, index) { return 3; }
  );
